import h5py
import psycopg2
import numpy as np
import time
import codecs
import datetime
from io import StringIO


class Hdf5ToPostgres:
    def __init__(self, _file_name):
        self.file_name = _file_name
        self.file = self.get_file()
        self.conn = psycopg2.connect(database='hdf5', user='postgres', password=' ')
        self.cur = self.conn.cursor()
        self.name = ''
        self.sessions_counter = ''
        self.sets_filter = {'templeft': 10, 'tempright': 10, 'dtempleft': 10, 'dtempright': 10, 'respleft': 12,
                            'respright': 12, 'pulsewaveleft': 12, 'pulsewaveright': 12}
        self.session_num = {}

    def check_database(self):
        start = time.time()
        self.cur.execute('DROP SCHEMA public CASCADE;')
        self.cur.execute('CREATE SCHEMA public;')
        print('Database is cleared ({} sec)'.format(round(time.time() - start, 2)))

    def create_ids_and_session(self):
        start = time.time()
        self.cur.execute('CREATE TABLE patients (patient_id INT PRIMARY KEY, first_name TEXT, last_name TEXT, '
                         'gender TEXT, birth DATE);')
        for idents in self.file['Identificator'].keys():
            data_set = self.file['Identificator/{}'.format(idents)]
            first_name = self.decode(data_set[:, 0])
            last_name = self.decode(data_set[:, 1])
            gender = 'Male' if self.decode(data_set[:, 3]) else 'Female'
            birth_date = self.decode(data_set[:, 4])
            self.cur.execute('INSERT INTO patients VALUES (%s, %s, %s, %s, %s);',
                             (idents, first_name, last_name, gender, birth_date))
        print('Ids created. ({} sec)'.format(round(time.time() - start, 2)))
        self.cur.execute('CREATE TABLE sessions (session_id INT PRIMARY KEY, '
                         'patient_id INT REFERENCES patients(patient_id), period DATE, hours TIME);')

    def fill_database(self, dog):
        self.name = dog.name.split('/')
        if isinstance(dog, h5py.Dataset):
            if len(self.name) == 4:
                self.create_patient_snapshots(dog)
            if len(self.name) == 5:
                self.create_snapshot_category(dog)
            if len(self.name) == 6:
                self.create_measurements(dog)

        if isinstance(dog, h5py.Group):
            if isinstance(list(dog.values())[0], h5py.Dataset):
                self.check_session()
            for k, v in sorted(dict(dog).items()):
                self.fill_database(v)

    def get_file(self):
        try:
            return h5py.File(self.file_name, 'r')
        except OSError:
            return 'File not found.'

    @staticmethod
    def decode(column):
        return codecs.decode(bytes(column[np.where((column > 0) & (column < 256))].astype(int).tolist()), 'cp1140')

    def add_session_period(self, data_set):
        period = datetime.date(*data_set[:3].astype(int).tolist())
        self.cur.execute("UPDATE sessions SET period=(%s) WHERE session_id=%s;",
                         (period, self.session_num[self.name[3]]))

    def add_session_hours(self, data_set):
        hours = datetime.time(*data_set[:3].astype(int).tolist())
        self.cur.execute('UPDATE sessions SET hours=(%s) WHERE session_id=%s;', (hours, self.session_num[self.name[3]]))

    def get_patient_name(self):
        self.cur.execute('SELECT first_name FROM patients WHERE patient_id=%s;', (self.name[2],))
        return self.cur.fetchone()[0]

    def get_data(self, data_set):
        session = self.name[4]
        return StringIO('{}\t{}\t{}\t{}\t{{{}}}'.format(self.session_num[self.name[3]], self.name[2], session,
                                                        self.get_patient_name(), self.get_str_data(data_set)))

    @staticmethod
    def get_str_data(data_set):
        data = data_set.value.astype(int).tolist()
        return ','.join(map(str, data)).replace('[', '{').replace(']', '}')

    def check_session(self):
        if self.name[3] in self.session_num:
            self.session_num[self.name[3]] += 1
        else:
            self.session_num[self.name[3]] = 1
        if not self.sessions_counter:
            self.sessions_counter = self.name[3]  # stage for total session count
        if self.sessions_counter in self.name:
            self.cur.execute("INSERT INTO sessions VALUES (%s, %s);",
                             (self.session_num[self.sessions_counter], self.name[2]))

    def create_patient_snapshots(self, data_set):
        self.cur.execute('CREATE TABLE IF NOT EXISTS patient_snapshots ('
                         'patient_id INT REFERENCES patients(patient_id), first_name TEXT, data INTEGER[]);')
        self.cur.execute('INSERT INTO patient_snapshots VALUES (%s, %s, ARRAY[{}]);'
                         .format(self.get_str_data(data_set)), (self.name[2], self.get_patient_name()))

    def create_snapshot_category(self, data_set):
        table_name = '{}_snapshot_category'.format(self.name[3])
        self.cur.execute('CREATE TABLE IF NOT EXISTS {} ('
                         'session_id INT PRIMARY KEY REFERENCES sessions(session_id), '
                         'patient_id INT REFERENCES patients(patient_id), first_name TEXT, data INTEGER[]);'
                         .format(table_name))
        self.cur.execute('INSERT INTO {} VALUES (%s, %s, %s, ARRAY[{}]);'
                         .format(table_name, self.get_str_data(data_set)),
                         (self.session_num[self.name[3]], self.name[2], self.get_patient_name()))

    def create_measurements(self, data_set):
        start = time.time()
        table_name = '{}_{}'.format(self.name[3], self.name[5])
        self.cur.execute('CREATE TABLE IF NOT EXISTS {} ('
                         'session_id INT PRIMARY KEY REFERENCES sessions(session_id), '
                         'patient_id INT REFERENCES patients(patient_id), patient_session_id INT, '
                         'first_name TEXT, data BIGINT[]);'.format(table_name))
        if self.name[-1] not in self.sets_filter or data_set.shape[0] / 2 > (data_set[:] == 0).sum() + (
                    data_set[:] == 2 ** self.sets_filter[self.name[-1]] - 1).sum():
            self.cur.copy_from(self.get_data(data_set), table_name)
        if 'templeft' in self.name:
            self.add_session_period(data_set)
        if 'tempright' in self.name:
            self.add_session_hours(data_set)
        print('Ready: {}[{}] ({} sec)'.format(table_name, data_set.shape[0], round(time.time() - start, 2)))

    def start(self):
        start = time.time()
        if not isinstance(self.file, h5py.File):
            return self.file
        self.check_database()
        self.create_ids_and_session()
        self.fill_database(self.file['PersonalData'])
        self.conn.commit()
        runtime = time.time() - start
        return 'Database is filled. The runtime took {} seconds to complete.'.format(round(runtime, 2))


if __name__ == '__main__':
    print(Hdf5ToPostgres('db.hdf5').start())
